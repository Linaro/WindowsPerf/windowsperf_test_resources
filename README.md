# Project Migration Notice

**Important:** This project has been moved to [GitHub](https://github.com/arm-developer-tools/windowsperf-test-resources). All development, new issues, pull requests, and releases are now being managed in the new repository.

## New Repository

You can find the new repository https://github.com/arm-developer-tools/windowsperf-test-resources.

## Old Project

The old project [README](README-old.md) is still available for reference in the GitLab repository.

## Active Development

Please note that this project is still actively developed and maintained. We encourage you to visit the new repository for the latest updates and contributions.

Thank you for your understanding and continued support!

-- 
WindowsPerf Team
